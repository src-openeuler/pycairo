Name:             pycairo
Version:          1.26.0
Release:          1
Summary:          A python module providing bindings for the cairo graphics library
License:          LGPL-2.1-only OR MPL-1.1
URL:              https://cairographics.org/pycairo
Source0:          https://github.com/pygobject/pycairo/releases/download/v%{version}/pycairo-%{version}.tar.gz

BuildRequires:    pkgconfig(cairo) gcc python3-devel python3-pytest python3-setuptools

%description
Pycairo is a Python module providing bindings for the cairo graphics library.

%package -n python3-cairo
Summary: Provide python3 support for the cairo graphics library
%{?python_provide:%python_provide python3-cairo}
Provides: python%{python3_version}dist(pycairo) = %{version}

%description -n python3-cairo
Provide python3 support for the cairo graphics library.


%package -n python3-cairo-devel
Summary: Provide libraries and headers for py3cairo
Requires: python3-cairo = %{version}-%{release}
Requires: python3-devel

%description -n python3-cairo-devel
Provide files for the use of building wrappers for cairo add-on libraries
in order to interoperate with py3cairo.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-cairo
%defattr(-,root,root)
%license COPYING*
%doc README.rst
%{python3_sitearch}/cairo/*
%{python3_sitearch}/pycairo*.egg-info/*

%files -n python3-cairo-devel
%defattr(-,root,root)
%{_includedir}/pycairo/py3cairo.h
%{_libdir}/pkgconfig/py*.pc     

%changelog
* Mon Mar 04 2024 wangqia <wangqia@uniontech.com> - 1.26.0-1
- Update to version 1.26.0

* Wed Dec 27 2023 konglidong <konglidong@uniontech.com> - 1.25.1-1
- update version to 1.25.1

* Mon Jul 24 2023 yanglu <yanglu72@h-partners> - 1.24.0-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update pycairo version to 1.24.0

* Fri Mar 10 2023 yanglu <yanglu72@h-partners> - 1.23.0-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update pycairo version to 1.23.0

* Mon Oct 24 2022 yanglu <yanglu72@h-partners> - 1.21.0-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update pycairo version to 1.21.0

* Thu Jul 21 2022 jinzhiguang <jinzhiguang@kylinos.cn> - 1.20.0-2
- Type:requirements
- Id:NA
- SUG:NA
- DESC:Add provides for python3-cairo

* Sat Jan 30 2021 xihaochen <xihaochen@huawei.com> - 1.20.0-1
- Type:requirements                                                                                                                                  
- Id:NA
- SUG:NA
- DESC:update pycairo to 1.20.0

* Thu Oct 29 2020 gaihuiying <gaihuiying1@huawei.com> - 1.19.1-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove python2

* Wed Jul 29 2020 gaihuiying <gaihuiying1@huawei.com> - 1.19.1-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update pycairo version to 1.19.1

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.18.2-1
- update to 1.18.2

* Mon Oct 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.18.1-2
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:Delete extra infomation of package python3-cairo-devel

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.18.1-1
- Package init
